package com.instreamatic.adman.demo.base;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.view.IAdmanView;


public class DemoAdmanActivity extends Activity implements AdmanEvent.Listener {

    final private static String TAG = "DemoAdman";

    private IAdman adman;

    private View startView;
    private View loadingView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_adman_activity);
        AdmanRequest request = new AdmanRequest.Builder()
                .setSiteId(1249)
                .setRegion(Region.GLOBAL)
                .build();
        adman = new Adman(this, request);
        adman.bindModule(new CustomAdmanView(this));
        adman.addListener(this);

        startView = findViewById(R.id.start);
        loadingView = findViewById(R.id.loading);
        startView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adman.start();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        adman.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adman.play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adman.removeListener(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        ((IAdmanView) adman.getModule(IAdmanView.ID)).rebuild();
    }

    @Override
    public void onAdmanEvent(final AdmanEvent event) {
        Log.d(TAG, "onAdmanEvent: " + event.getType().name());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (event.getType()) {
                    case PREPARE:
                        startView.setVisibility(View.GONE);
                        loadingView.setVisibility(View.VISIBLE);
                        break;
                    case NONE:
                    case FAILED:
                    case COMPLETED:
                        startView.setVisibility(View.VISIBLE);
                        loadingView.setVisibility(View.GONE);
                        break;
                    case STARTED:
                        loadingView.setVisibility(View.GONE);
                        break;
                }
            }
        });
    }
}
