package com.instreamatic.adman.demo.base;

import android.app.Activity;
import android.content.Context;

import com.instreamatic.adman.view.AdmanViewType;
import com.instreamatic.adman.view.IAdmanViewBundle;
import com.instreamatic.adman.view.core.AdmanViewBindFactory;
import com.instreamatic.adman.view.core.AdmanViewBundle;

import java.util.HashMap;
import java.util.Map;

public class CustomAdmanViewBindFactory extends AdmanViewBindFactory {


    final Map<AdmanViewType, Integer> bindings =  new HashMap<AdmanViewType, Integer>() {{
            put(AdmanViewType.BANNER, R.id.adman_banner);
            put(AdmanViewType.RESTART, R.id.adman_restart);
            put(AdmanViewType.PLAY, R.id.adman_play);
            put(AdmanViewType.PAUSE, R.id.adman_pause);
            put(AdmanViewType.LEFT, R.id.adman_left);
            put(AdmanViewType.CLOSE, R.id.adman_close);

        }};


    @Override
    protected IAdmanViewBundle buildPortrait(Activity context) {
        return AdmanViewBundle.fromLayout(context, R.layout.demo_adman_custom_activity, bindings);
    }

    @Override
    protected IAdmanViewBundle buildLandscape(Activity context) {
        return AdmanViewBundle.fromLayout(context, R.layout.demo_adman_custom_activity, bindings);
    }

    @Override
    protected IAdmanViewBundle buildVoice(Activity context) {
        return AdmanViewBundle.fromLayout(context, R.layout.demo_adman_custom_activity, bindings);
    }

}