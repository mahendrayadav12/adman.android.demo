package com.instreamatic.adman.demo.voice;

import android.app.Activity;
import com.instreamatic.adman.view.IAdmanViewBundleFactory;
import com.instreamatic.adman.view.generic.DefaultAdmanView;

public class DemoVoiceAdmanView extends DefaultAdmanView{

    private IAdmanViewBundleFactory factory;

    public DemoVoiceAdmanView(final Activity context) {
        super(context);
        factory = new DemoVoiceAdmanViewBindFactory();
    }

    @Override
    public IAdmanViewBundleFactory factory() {
        return factory;
    }

}